import React from 'react'
import  UserList from './components/UserList'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const App = () => {
  return (
    <Router>
     
     <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <Link class="navbar-brand" to="/">Home</Link>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <Link class="nav-link" to="/register">Registro</Link>
      </li>
      <li class="nav-item active">
      <Link class="nav-link" to="/listuser">Listar</Link>
      </li>

    </ul>
    
  </div>
</nav>

    <div class="container">
       
      <Switch>
          <Route path="/listuser">
          <UserList/>
          </Route>
          <Route path="/register">
          </Route>
          <Route path="/">

          </Route>
      </Switch>
  
      
      
  
   
   </div>
   </Router>
  );
}

export default App;
