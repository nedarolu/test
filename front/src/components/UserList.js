import React, { useState, useEffect } from "react";
const UserList= () => {
  const [data, setData]=  useState([])
  const url = 'http://localhost:5000/api/v1/persons'
  useEffect(() => {
      const fetchData = async() => {
        await fetch (url)
        .then(response => response.json())
        .then(json=> setData(json))
        .catch(error => console.log(error))
      }
    fetchData()
},[])
return(

  <table class="table table-striped">
    <thead>
    <td>Número Documento</td>
    <td>Nombre</td>
    <td>Apellido</td>
    <td>Usuario</td>
    <td>Estado</td>
    </thead>
    <tbody>
    
   {data.map((item,i)=> (
   <tr key={i} >

   <td>{item.identification_number}</td>  
   <td>{item.name}</td>  
   <td>{item.last_name}</td>
   <td>{item.user}</td>
   <td>{ item.status== 1 ? 'Activo':'Inactivo'}</td>
  </tr>
 

  ))}

  </tbody>
  </table>


);





}

export default UserList;