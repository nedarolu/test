from werkzeug.security import generate_password_hash, check_password_hash
from app import  db,ma

class  Type_identification(db.Model):
    id = db.Column(db.Integer,primary_key=True, nullable=False)
    description = db.Column(db.String(40),nullable=False)
    rs_type= db.relationship('Person', backref='type')

    def __init__(self,  description, code):
           self.description = description
           self.id = id

class Person(db.Model):
    id = db.Column(db.Integer,autoincrement=True)
    identification_number = db.Column(db.Integer, primary_key=True, unique=True )
    name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100),  nullable=False)
    birth_date= db.Column(db.String(48),  nullable=False)
    status = db.Column(db.Boolean, default=True,nullable=False)
    user= db.Column(db.String(48),  nullable=False)
    password= db.Column(db.String(150),  nullable=False)
    type_identification = db.Column(db.Integer, db.ForeignKey('type_identification.id'), nullable=False)

    def __init__(self, type_identification, identification_number, name, last_name, birth_date, user, password):
           self.type_identification= type_identification
           self.identification_number= identification_number
           self.name = name
           self.last_name = last_name
           self.birth_date = birth_date
           self.user = user
           self.password= generate_password_hash(str(password))

    def update(self, name, last_name,birth_date,status):
           self.name = name
           self.last_name = last_name
           self.birth_date = birth_date
           self.status = status

    def verify_password(self, password):
       return check_password_hash(self.password.strip(), password)



class personSchema(ma.Schema):
    class Meta:
      fields = ('identification_number','type.description','identification','name','last_name', 'birth_date', 'status', 'user')



