from flask import Flask, request,jsonify
from werkzeug.security import generate_password_hash, check_password_hash
from flask_mysqldb import MySQL
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

#connection data  mysql
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://test:Test.3567@localhost/test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
mysql = MySQL(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)
CORS(app)

from models import  Person, personSchema
personSchema = personSchema(many=True)

#person
@app.route('/api/v1/persons', methods=['GET','POST'])
def all_person():
   if request.method== 'GET':
      data = Person.query.all()
      return personSchema.jsonify(data)

   if request.method== 'POST':
      type_identification=request.json['type_identification']
      identification_number= request.json['identification_number']
      name= request.json['name']
      last_name= request.json['last_name']
      birth_date= request.json['birth_date']
      user= request.json['user']
      password= request.json['password']
      
      check_identification= Person.query.filter_by(identification_number=identification_number).first()
      check_person=Person.query.filter_by(user=user).first()
      verify= False
      if check_person:
          verify=True
          check={"message": "This user has already been registered"}
      if check_identification:
          verify=True
          check={"message": "This identification has already been registered"}
      if not verify:
         person= Person(type_identification, identification_number, name, last_name, birth_date, user, password)
         db.session.add(person)
         db.session.commit()
         return jsonify(),201
      else:
          return jsonify(check), 400

#person id
@app.route('/api/v1/persons/<int:id>', methods=['GET'])
def get_person(id):
    if request.method== 'GET':
       data = Person.query.filter_by(identification_number=id)
       return personSchema.jsonify(data)

#update person 
@app.route('/api/v1/persons/<int:id>', methods=['PUT'])
def update_person(id):
    if request.method== 'PUT':
        name= request.json['name']
        last_name= request.json['last_name']
        birth_date= request.json['birth_date']
        status= request.json['status']
        person   = Person.query.get(id)
        if person is  not None:
          person.update(name,last_name,birth_date,status)
          db.session.add(person)
          db.session.commit()
          return jsonify({'message': "modified record"})
        else:
          return jsonify({'message': "null record"})

#login person
@app.route('/api/v1/persons/login', methods=['POST'])
def login_person():
  if request.method == 'POST':
    user= request.json['user']
    password= request.json['password']
    person = Person.query.filter_by(user=user).first()
    if person is not None and person.verify_password(str(password)):
      return jsonify({"menssage":"Person autenticate" })
    else:
     return jsonify({"message":"Invalid data" })

if __name__ ==  "__main__":
    app.run(debug=True)
